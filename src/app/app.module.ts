import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { TabsPage } from '../modules/tabs/tabs';

import { ShowsController } from '../modules/shows/controllers/shows.controller';
import { ViewShowController } from '../modules/shows/controllers/view.controller'
import { SongsController } from '../modules/songs/controllers/songs.controller';
import { ViewSongController } from '../modules/songs/controllers/view.controller';

import { ShowService } from './services/show.service';
import { SongService } from './services/song.service';

@NgModule({
  declarations: [
    MyApp,
    ShowsController,
    SongsController,
    ViewShowController,
    ViewSongController,
    TabsPage
  ],
  imports: [
    IonicModule.forRoot(MyApp, {
      tabsHideOnSubPages: true
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ShowsController,
    ViewShowController,
    SongsController,
    ViewSongController,
    TabsPage
  ],
  providers: [
    ShowService,
    SongService
  ]
})
export class AppModule {}
