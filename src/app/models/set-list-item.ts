export class SetListItem {
	title: string;
	artist: string;
	BPM: string;
	isSong: boolean;
}