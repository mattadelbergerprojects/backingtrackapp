import { Injectable } from '@angular/core';
import * as PouchDB from 'pouchdb';

@Injectable()
export class ShowService {
    private _db;
    private _shows;

    initDB() {
        this._db = new PouchDB('bt_shows', { adapter: 'websql' });
    }

    add(show) {
      return this._db.post(show);
    } 

    update(show) {  
      return this._db.put(show);
    }

    delete(show) {
      return this._db.remove(show);
    }

    getAll() {  
      
      return this._db.allDocs({ include_docs: true})
        .then(docs => {
          this._shows = docs.rows.map(row => {
              return row.doc;
          });

          // Listen for changes on the database.
          this._db.changes({ live: true, since: 'now', include_docs: true})
              .on('change', this.onDatabaseChange);
          return this._shows;
        });
  }

  private onDatabaseChange = (change) => {  
    var index = this.findIndex(this._shows, change.id);
    var show = this._shows[index];

    if (change.deleted) {
        if (show) {
            this._shows.splice(index, 1); // delete
        }
    } else {
        if (show && show._id === change.id) {
            this._shows[index] = change.doc; // update
        } else {
            this._shows.splice(index, 0, change.doc) // insert
        }
    }
  }

  // Binary search, the array is by default sorted by _id.
  private findIndex(array, id) {  
      var low = 0, high = array.length, mid;
      while (low < high) {
      mid = (low + high) >>> 1;
      array[mid]._id < id ? low = mid + 1 : high = mid
      }
      return low;
  }
}