import { Injectable } from '@angular/core';
import * as PouchDB from 'pouchdb';

@Injectable()
export class SongService {
    private _db;
    private _songs;

    initDB() {
        this._db = new PouchDB('bt_songs', { adapter: 'websql' });
    }

    add(song) {
      return this._db.post(song);
    } 

    update(song) {  
      return this._db.put(song);
    }

    delete(song) {
      return this._db.remove(song);
    }

    getAll() {  

      return this._db.allDocs({ include_docs: true})
       .then(docs => {
          this._songs = docs.rows.map(row => {
              return row.doc;
          });

          // Listen for changes on the database.
          this._db.changes({ live: true, since: 'now', include_docs: true})
              .on('change', this.onDatabaseChange);
          return this._songs;
        });
  }

  private onDatabaseChange = (change) => {  
    var index = this.findIndex(this._songs, change.id);
    var song = this._songs[index];

    if (change.deleted) {
        if (song) {
            this._songs.splice(index, 1); // delete
        }
    } else {
        if (song && song._id === change.id) {
            this._songs[index] = change.doc; // update
        } else {
            this._songs.splice(index, 0, change.doc) // insert
        }
    }
  }

  // Binary search, the array is by default sorted by _id.
  private findIndex(array, id) {  
      var low = 0, high = array.length, mid;
      while (low < high) {
      mid = (low + high) >>> 1;
      array[mid]._id < id ? low = mid + 1 : high = mid
      }
      return low;
  }
}