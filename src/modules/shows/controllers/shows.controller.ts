import { Component, NgZone } from "@angular/core";  
import { NavController, Platform, ActionSheetController } from 'ionic-angular';
import { ShowService } from '../../../app/services/show.service'; 
import { ViewShowController } from './view.controller';

@Component({
  selector: 'shows',
  templateUrl: '../templates/shows.template.html'
})

export class ShowsController {

	public shows = [];

	constructor(
		private nav: NavController,
		private showService: ShowService,
		private platform: Platform,
		private zone: NgZone,
    private actionSheet: ActionSheetController
	) {}

	ionViewDidLoad() {
		this.showService.initDB();
		
    this.platform.ready().then(() => {
      this.showService.getAll()
        .then(data => {
            this.zone.run(() => {
                this.shows = data;
            });
        })
        .catch(console.error.bind(console));
    });
  }

  openActionSheet(show) {
    let actionSheet = this.actionSheet.create({
      title: 'Show',
      buttons: [
        {
          text: 'Start Show',
          handler: () => {
            
          }
        },
        {
          text: 'Edit',
          handler: () => {
            this.viewShow(show);
          }
        },{
          text: 'Delete',
          role: 'destructive',
          handler: () => {
            this.deleteShow(show);
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {

          }
        }
      ]
    });
    actionSheet.present();
  }

  addShow() {
    this.nav.push(ViewShowController);
  }

  viewShow(show) {
    this.nav.push(ViewShowController, show);
  }

	deleteShow(show) {
    this.showService.delete(show).then(data => {
      console.log("Successfully Deleted " + data.name)
    })
    .catch(console.error.bind(console));
  }
}