import { Component, NgZone } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { ShowService } from '../../../app/services/show.service';
import { SongService } from '../../../app/services/song.service';
import { SetListItem } from '../../../app/models/set-list-item';

@Component({
	selector: 'view',
  templateUrl: '../templates/view.template.html'
})

export class ViewShowController {

  private showExists = false;

  public show;
  public pageTitle = 'New Show';

  public setList;
  public numberOfSets = 0;
  public numberOfSongs = 0;

  constructor(private showService: ShowService,
    private platform: Platform,
    private zone: NgZone,
    private songService: SongService,
  	private formBuilder: FormBuilder,
  	private nav: NavController,
    private navParams: NavParams
  	) {

    if (navParams.data._id) {
      this.showExists = true;
      this.pageTitle = 'Edit Show';
      this.setList = navParams.data.setList;
      this.numberOfSets = navParams.data.numberOfSets;
    } else {

      this.songService.initDB();

      platform.ready().then(() => {

        this.songService.getAll()
          .then(data => {
              this.zone.run(() => {
                  this.setList = data;
                  for (var i = this.setList.length - 1; i >= 0; i--) {
                    this.setList[i].isSong = true;
                  }

                  let backlog = new SetListItem();
                  backlog.title = 'Backlog';
                  backlog.isSong = false;

                  this.setList.unshift(backlog);

                  this.show.patchValue({setList: this.setList})
              });
          })
          .catch(console.error.bind(console));
      });
    }
  }
  
  ionViewDidLoad() {
    this.show = this.formBuilder.group({
      _id: [this.navParams.data._id],
      _rev: [this.navParams.data._rev],
      name: [this.navParams.data.name, Validators.required],
      setList: [this.setList],
      numberOfSets: [this.numberOfSets],
      numberOfSongs: [this.numberOfSongs]
    });
  }

  addSet() {
    var set = new SetListItem();

    this.numberOfSets++;
    set.title = 'Set ' + (this.numberOfSets);
    set.isSong = false;

    this.setList.splice(0, 0, set);
    this.show.patchValue({numberOfSets: this.numberOfSets})
  }

  reorderSetListItem(indexes){
    let element = this.setList[indexes.from];
    this.setList.splice(indexes.from, 1);
    this.setList.splice(indexes.to, 0, element);
  }

  removeSetListItem(item) {
    var index = this.setList.indexOf(item);
    this.setList.splice(index, 1);
    this.numberOfSets--;
    this.show.patchValue({numberOfSets: this.numberOfSets})
  }

  saveShow() {
    this.numberOfSongs = 0;
    
    let i = 0;
    while (this.setList[i].title != 'Backlog')
    {
      if(this.setList[i].title.indexOf('Set') != 0) {
        this.numberOfSongs = this.numberOfSongs + 1;
      }
      i++;
    };

    this.show.patchValue({numberOfSongs: this.numberOfSongs});

    if (this.showExists) {
      this.showService.update(this.show.value).then((data) => {
      this.nav.pop();
      }).catch((ex) => {
        console.error('Error updating show', ex);
      }); 
    } else {
      this.showService.add(this.show.value).then((data) => {
      this.nav.pop();
      }).catch((ex) => {
        console.error('Error saving show', ex);
      });
    }
  }
}