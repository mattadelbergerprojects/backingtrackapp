import { Component, NgZone } from "@angular/core";  
import { NavController, Platform, ActionSheetController } from 'ionic-angular';
import { MediaPlugin} from 'ionic-native';
import { SongService } from '../../../app/services/song.service';
import { ViewSongController } from './view.controller';

declare var cordova: any;

@Component({
  selector: 'songs',
  templateUrl: '../templates/songs.template.html'
})

export class SongsController {

	public appRootFolder;
	public file;

	public setList = [];
	public songs = [];

	constructor(
		private nav: NavController,
		private platform: Platform,
		private songService: SongService,
		private zone: NgZone,
		private actionSheet: ActionSheetController
	) {

			this.songService.initDB();

			platform.ready().then(() => {

				this.songService.getAll()
	        .then(data => {
	            this.zone.run(() => {
	                this.songs = data;
	            });
	        })
	        .catch(console.error.bind(console));

		    this.appRootFolder = cordova.file.documentsDirectory;
		    this.appRootFolder = this.appRootFolder.replace('file://', '');

		  });
		}

	openActionSheet(song) {
    let actionSheet = this.actionSheet.create({
      title: song.filename,
      buttons: [
        {
          text: 'Preview Song',
          handler: () => {
            this.play(song);
          }
        },
        {
          text: 'Edit',
          handler: () => {
            this.viewSong(song);
          }
        },{
          text: 'Delete',
          role: 'destructive',
          handler: () => {
    				this.songService.delete(song).then(data => {
      				console.log("Successfully Deleted Song")
    				})
    				.catch(console.error.bind(console));
          	}
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {

          }
        }
      ]
    });
    actionSheet.present();
  }

  addSong() {
  	this.nav.push(ViewSongController);
  }

  viewSong(song) {
    this.nav.push(ViewSongController, song);
  }

	play(song) {
		if(this.file != undefined) {
			this.file.release();
		}

		this.file = new MediaPlugin(this.appRootFolder + song.filename);
		this.file.play();
	}

	stop() {
		this.file.release();
	}
}