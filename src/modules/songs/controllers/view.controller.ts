import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { SongService } from '../../../app/services/song.service';

declare var iOSAudioPicker: any;
declare var cordova: any;

@Component({
	selector: 'view',
  templateUrl: '../templates/view.template.html'
})

export class ViewSongController {

  private songExists = false;

  public song;
  public pageTitle = 'New Song';

  constructor(private songService: SongService,
  	private formBuilder: FormBuilder,
  	private nav: NavController,
    private navParams: NavParams
  	) {

    if (navParams.data._id) {
      this.songExists = true;
      this.pageTitle = 'Edit Song';
    }
  }
  
  ionViewDidLoad() {
    this.song = this.formBuilder.group({
      _id: [this.navParams.data._id],
      _rev: [this.navParams.data._rev],
      title: [this.navParams.data.title, Validators.required],
      artist: [this.navParams.data.artist, Validators.required],
      bpm: [this.navParams.data.bpm, Validators.required],
      filename: [this.navParams.data.filename]
    });
  }

  getSongFromDevice() {
    window['plugins'].iOSAudioPicker.getAudio(this.addSong(this.song) ,this.error,"false","false");
  }

  addSong(song): any {
    var success = function(data) {
        song.setValue({filename: data[0].filename})
      };
      return success;
  }

  error(e) {
    console.log(e);
  }

  saveSong() {
    if (this.songExists) {
      this.songService.update(this.song.value).then((data) => {
      this.nav.pop();
      }).catch((ex) => {
        console.error('Error updating song', ex);
      }); 
    } else {
      this.songService.add(this.song.value).then((data) => {
      this.nav.pop();
      }).catch((ex) => {
        console.error('Error saving song', ex);
      });
    }
  }
}