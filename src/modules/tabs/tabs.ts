import { Component } from '@angular/core';

import { ShowsController } from '../shows/controllers/shows.controller';
import { SongsController } from '../songs/controllers/songs.controller';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  tab1Root: any = ShowsController;
  tab2Root: any = SongsController;

  constructor() {
 
  }
}
